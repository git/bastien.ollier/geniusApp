import { FETCH_ARTISTE_SEARCH} from '../constants';

const initialState = {
    artistsSearch: []
}

export default function ReducerArtist(state = initialState, action){
    switch (action.type) {
        case FETCH_ARTISTE_SEARCH:
            return {...state, artistsSearch: action.payload};
        default:
            console.log("nothing")
            return state;
    }
}