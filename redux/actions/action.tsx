import {FETCH_ARTISTE_SEARCH, ACCESS_TOKEN} from '../constants';
import {Artist} from "../../Model/Artist";
import {Album} from "../../Model/Album";


export const setArtistSearchList = (artistList: Artist[]) => {
    return {
        type: FETCH_ARTISTE_SEARCH,
        payload: artistList,
    };
}

export const searchArtists = (recherche) => {
    return async dispatch => {
        try{
                const response = await fetch(`https://genius.com/api/search/artists?page=1&q=${encodeURIComponent(recherche)}`);

                if (!response.ok) {
                    throw new Error('[searchArtists] Network response was not ok');
                }

                const artistListJson = await response.json();

                const artistList: Artist[] = artistListJson.response.sections[0].hits.map(hit =>
                    new Artist(hit.result.id, hit.result.name, hit.result.image_url,"")
                );

                dispatch(setArtistSearchList(artistList));

        } catch (error) {
                console.error('Error:', error);
            }
    }
}

export const getArtistInfo = (artist) => {
    return async (dispatch) => {
        try {
            const response = await fetch(
                `https://api.genius.com/artists/${artist.id}`,
                {
                    headers: {
                        Authorization: `Bearer ${ACCESS_TOKEN}`,
                        "Content-Type": "application/json",
                    },
                }
            );

            if (!response.ok) {
                console.log(response);
                throw new Error("[getArtistInfo] Network response was not ok");
            }

            const artistInfoJson = await response.json();

            var bio = "";
            artistInfoJson.response.artist.description.dom.children[0].children.map(
                (b) => {
                    if (typeof b === "object") {
                        bio += b.children[0];
                    } else {
                        bio += b;
                    }
                }
            );

            return bio; // Return the JSON object
        } catch (error) {
            console.error("Error:", error);
        }
    };
};

export const getAlbumByArtist = (artist) => {
    return async dispatch => {
        try{
            const response = await fetch(`https://genius.com/api/artists/${artist.id}/albums`)

            if (!response.ok) {
                console.log(response);
                throw new Error('[getAlbumByArtist] Network response was not ok');
            }

            const artistAlbumJson = await response.json();

            const albumList: Album[] = artistAlbumJson.response.albums.map((album: any) =>
                new Album(album.id,
                    album.name,
                    album.cover_art_thumbnail_url,
                    // @ts-ignore
                    new Date(album.release_date_components.year, album.release_date_components.month, album.release_date_components.day)
                )
            );

            return albumList;
        } catch (error) {
            console.error('Error:', error);
        }
    }
}