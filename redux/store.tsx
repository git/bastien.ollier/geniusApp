import {configureStore} from '@reduxjs/toolkit'
import ReducerArtist from './reducers/reducerArtist';

// Reference here all your application reducers
const reducer = {
    ReducerArtist: ReducerArtist,
}

const store = configureStore(
    {
        reducer,
        middleware: (getDefaultMiddleware ) => getDefaultMiddleware({serializableCheck : false})
    } );

export default store;