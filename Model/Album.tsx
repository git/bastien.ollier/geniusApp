export class Album{
    private _id: number;
    private _name: string;
    private _coverUrl: string
    private _date: Date;

    constructor(id: number, name: string, coverUrl: string, date: Date) {
        this._id = id;
        this._name = name;
        this._coverUrl = coverUrl;
        this._date = date;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get coverUrl(): string {
        return this._coverUrl;
    }

    set coverUrl(value: string) {
        this._coverUrl = value;
    }

    get date(): Date {
        return this._date;
    }

    set date(value: Date) {
        this._date = value;
    }
}