# GeniusAPP

Cette application React Native permet aux utilisateurs de rechercher un artiste et de visualiser sa biographie et la liste de ses albums( avec la date de parution). Elle utilise l'API de Genius.com pour récupérer les informations sur les artistes et les albums.

## Fonctionnalité
- Recherche d'artiste par leur nom
- Consultation d'une courte biographie de l'artiste ainsi que de la liste de ses albums
- Possibilité de liker un artiste pour y a accéder plus rapidement via la home page

## Utilisation/Navigation

### Page de recherche
<img src="imageReadme/search.jpg" width="216" height="480" />

La recherche d'un artiste dans cette application est un processus simple et intuitif. L'utilisateur peut saisir le nom de l'artiste dans la barre de recherche et appuyer sur le bouton de recherche pour afficher les résultats.

### Page d'informations sur un artiste
<img src="imageReadme/EminemUnlike.jpg" width="216" height="480" />

Cette page présente une biographie concise et détaillée de l'artiste, ainsi que la liste de ses albums avec leur date de parution

### like artiste
<img src="imageReadme/EminemLike.jpg" width="216" height="480" />

il peut aimer l'artiste en cliquant sur le bouton de like. Cette action ajoute l'artiste à une liste de favoris dans l'application qui se situe dans la HomePage

### home page avec la liste des artistes favorie
<img src="imageReadme/homePage.jpg" width="216" height="480" />


## Contributeur
Bastien Ollier PM2
