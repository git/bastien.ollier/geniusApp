import {SafeAreaView, StyleSheet} from 'react-native';
import React from "react";
import NavigationBar from "./componente/Navigation/NavigationBar";
import {Provider} from "react-redux";

import store from "./redux/store";

export default function App() {
    return (
    <>
        <Provider store={store}>
            <SafeAreaView style={styles.topSafeArea}/>
            <SafeAreaView style={styles.mainSafeArea}>
                <NavigationBar/>
            </SafeAreaView>
        </Provider>
    </>
  );
}

const styles = StyleSheet.create({
    mainSafeArea: {
        flex: 1,
        backgroundColor: "#7a96e9"
    },
    topSafeArea: {
        flex: 0,
        backgroundColor: 'darksalmon'
    }
});
