import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import ArtistList from "./Artist/ArtistList";

const HomePage = ({navigation}) => {

    return (
        <View style={styles.container}>
            <Text style={styles.titre}>Artiste favori</Text>
            <ArtistList navigation={navigation} type={"liked"}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop:40,
        marginLeft:20
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    titre: {
        fontSize: 20,
        fontWeight: "bold"
    }
});

export default HomePage;
