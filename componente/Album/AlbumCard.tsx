// @ts-ignore
import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';




const AlbumCard = ({ album }) => {
    const date = new Date(album.date);
    const formattedDate = date.toLocaleDateString('en-US', { month: 'long', day: 'numeric', year: 'numeric' });

    return (
        <View style={styles.container}>
            <Image source={{ uri: album.coverUrl }} style={styles.image}/>
            <View style={styles.containerText}>
                <Text style={styles.name}>{album.name}</Text>
                <Text>{formattedDate}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        margin: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    containerText:{
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    image: {
        width: 50,
        height: 50,
        marginRight: 10,
    },
});

export default AlbumCard;
