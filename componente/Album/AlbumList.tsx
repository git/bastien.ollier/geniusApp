import React from "react";
import {FlatList, ScrollView, StyleSheet, View} from "react-native";
import AlbumCard from "./AlbumCard";

const ArtistList = ({ALBUM_LIST}) => {
    return (
        <View style={styles.container}>
            <FlatList data={ALBUM_LIST} renderItem={({ item }) => (
                    <AlbumCard key={item.id} album={item} />
                )}
            />
        </View>

    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '70%',
    }
});

export default ArtistList;


