import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const SettingsPage = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.name}>Settings</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop:40,
        alignItems: 'center'

    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});

export default SettingsPage;
