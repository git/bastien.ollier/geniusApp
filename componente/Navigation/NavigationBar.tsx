import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {NavigationContainer} from "@react-navigation/native";
import {AntDesign, Ionicons} from '@expo/vector-icons';
import SettingsPage from "../SettingsPage";
import StackNavigationHomePage from "./StackNavigationHomePage";
import StackNavigationSearchPage from "./StackNavigationSearchPage";

function NavigationBar() {
    const BottomTabNavigator = createBottomTabNavigator();
    return (
        <NavigationContainer >
            <BottomTabNavigator.Navigator initialRouteName="HomeNav" screenOptions={{headerShown: false}}>
                <BottomTabNavigator.Screen name="HomeNav" component={StackNavigationHomePage} options={{
                    title: 'Home',
                    tabBarIcon: () => (<AntDesign name="home" size={24} color="black" />)
                }}/>
                <BottomTabNavigator.Screen name="Search" component={StackNavigationSearchPage} options={{
                    title: 'Search',
                    tabBarIcon: () => (<AntDesign name="search1" size={24} color="black" />)
                }}/>
            </BottomTabNavigator.Navigator>
        </NavigationContainer>
    )
}

export default NavigationBar;
