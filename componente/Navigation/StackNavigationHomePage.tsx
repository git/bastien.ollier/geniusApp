// @ts-ignore
import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import HomePage from "../HomePage";
import ArtistePage from "../Artist/ArtistePage";

const Stack = createStackNavigator();

export default function StackNavigationHomePage() {
    return (
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={HomePage}/>
                <Stack.Screen name="ArtistDetail" component={ArtistePage}/>
            </Stack.Navigator>
    )
}