// @ts-ignore
import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import HomePage from "../HomePage";
import ArtistePage from "../Artist/ArtistePage";
import SearchPage from "../SearchPage";

const Stack = createStackNavigator();

function StackNavigationSearchPage() {
    return (
        <Stack.Navigator initialRouteName="Home">
            <Stack.Screen name="Search" component={SearchPage}/>
            <Stack.Screen name="Detail sur un artiste" component={ArtistePage}/>
        </Stack.Navigator>
    )
}

export default StackNavigationSearchPage;