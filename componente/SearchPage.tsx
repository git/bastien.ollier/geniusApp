import React from "react";
import ArtistList from "./Artist/ArtistList";
import {View} from "react-native";
import SearchBar from "./SearchBar";


const SearchPage = ({ navigation }) => {
    return (
        <View>
            <SearchBar/>
            <ArtistList navigation={navigation} type={"search"}/>
        </View>
    );
};

export default SearchPage;


