import React, {useEffect, useState} from 'react';
import { TouchableOpacity, View } from 'react-native';
import {AntDesign} from "@expo/vector-icons";
import {addLikeArtist, getLikeArtist, removeArtist} from '../../AsyncStorage/likeArtistStorage';

const LikeButton = ({ artist }) => {
    const [isLiked, setIsLiked] = useState(false);
    const [likedArtists, setLikedArtists] = useState([]);

    useEffect(() => {
        const fetchLikedArtists = async () => {
            const artists = await getLikeArtist();
            setLikedArtists(artists);
            setIsLiked(artists.some((a) => a._id === artist._id));
        };
        fetchLikedArtists();
    }, [artist]);

    const handleLike = () => {
        setIsLiked(true);
        addLikeArtist(artist);
    };

    const handleDislike = () => {
        setIsLiked(false);
        removeArtist(artist._id);
    };

    return (
        <TouchableOpacity onPress={isLiked ? handleDislike : handleLike}>
            <View>
                <AntDesign name="heart" size={24} color={isLiked ? 'gold' : 'black'} />
            </View>
        </TouchableOpacity>
    );
}


export default LikeButton;