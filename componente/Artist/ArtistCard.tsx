import React from 'react';
import {View, Image, Text, StyleSheet, TouchableNativeFeedback, TouchableOpacity} from 'react-native';

const ArtistCard = ({ navigation, item }) => {
    return (
        <TouchableOpacity onPress={() => { console.log("ici");navigation.navigate("ArtistDetail",{"artist":item});console.log("non ici");}}>
            <View style={styles.container}>
                <Image source={{ uri: item.image }} style={styles.image}/>
                <Text style={styles.name}>{item.name}</Text>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 10,
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 40,
        marginRight: 10,
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
    },
});

export default ArtistCard;
