import React, {useEffect, useState} from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';
import {getAlbumByArtist, getArtistInfo} from "../../redux/actions/action";
import {useDispatch} from "react-redux";
import AlbumList from "../Album/AlbumList";
import LikeButton from "./likeButton";

export default function ArtistPage({ route }) {
    const [bio, setBio] = useState("");
    const [albumList, setAlbumList] = useState("");
    const artist = route.params.artist;
    const dispatch = useDispatch();

    useEffect(() => {
        const load = async () => {
            // @ts-ignore
            const bio = await dispatch(getArtistInfo(artist));
            if (bio) {
                // @ts-ignore
                setBio(bio);
            }

            // @ts-ignore
            const albumList = await dispatch(getAlbumByArtist(artist));
            if (albumList) {
                // @ts-ignore
                setAlbumList(albumList)
            }
        };
        load();
    }, [artist, dispatch])

    return (
        <View style={styles.container}>
            <Image source={{ uri: artist.image }} style={styles.image} />
            <View style={styles.likeButtonContainer}>
                <LikeButton artist={artist}/>
            </View>
            <Text style={styles.name}>{artist.name}</Text>
            <Text style={styles.bio}>{bio}</Text>

            <Text style={styles.titre}>Liste Album</Text>
            <AlbumList ALBUM_LIST={albumList}/>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        marginTop:10,
        alignItems: 'center',
        position: 'relative'
    },
    likeButtonContainer: {
        position: 'absolute',
        right: 10,
        backgroundColor: 'white',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: 'black',
        padding: 10,
        margin: 10
    },
    image: {
        width: 100,
        height: 100,
        marginBottom: 10,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: 'black'
    },
    name: {
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    bio:{

    },
    titre:{
        fontSize: 20,
        fontWeight: "bold"
    }
});
