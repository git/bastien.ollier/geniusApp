import React, {useEffect, useState} from "react";
import {FlatList, Text, View} from "react-native";
import {useSelector} from 'react-redux';
import ArtistCard from "./ArtistCard";
import {getLikeArtist} from './../../AsyncStorage/likeArtistStorage'

const ArtistList = ({ navigation, type }) => {
    const [artistsList, setArtistsList] = useState([]);

    const artists = useSelector((state) => {
        if (type === "search") {
            // @ts-ignore
            return state.ReducerArtist.artistsSearch;
        } else if (type === "liked") {
            // @ts-ignore
            return state.ReducerArtist.likedArtists;
        }
        return [];
    });

    useEffect(() => {
        if (type === "liked") {
            async function fetchLikedArtists() {
                const likedArtists = await getLikeArtist();
                setArtistsList(likedArtists);
            }
            fetchLikedArtists();
        }
    }, [type]);

    useEffect(() => {
        setArtistsList(artists);
    }, [artists]);

    if(artistsList == undefined){
         return <Text>Loading...</Text>
    }

    return (
        <View>
            <FlatList
                data={artistsList}
                renderItem={({ item }) => (
                    <ArtistCard key={item.id} navigation={navigation} item={item} />
                )}
                keyExtractor={(item) => item.id}
            />
        </View>
    );
};

export default ArtistList;


