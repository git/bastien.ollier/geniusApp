import React, { useState } from "react";
import { TextInput, TouchableOpacity, View, StyleSheet } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import {searchArtists} from "../redux/actions/action";
import {useDispatch, useSelector} from "react-redux";

const SearchBar = () => {
    const [searchText, setSearchText] = useState("");

    const dispatch = useDispatch();
    const handleSearch = () => {
        const loadArtist = async () => {
            // @ts-ignore
            await dispatch(searchArtists(searchText));
        };
        loadArtist();
    };

    return (
        <View style={styles.container}>
            <TextInput style={styles.input} value={searchText} onChangeText={setSearchText} placeholder="Search"/>
            <TouchableOpacity onPress={handleSearch}>
                <AntDesign name="search1" size={24} color="black" />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#FFFFFF",
        borderRadius: 5,
        margin: 10,
        paddingHorizontal: 5,
    },
    input: {
        flex: 1,
        fontSize: 18,
        paddingVertical: 10,
    },
});

export default SearchBar;